# Guest information Frontend 
```
Provides a frontend server for GuestInfoBackend using nginx 
```
## Project setup
## Using Yarn for Local Dynamic Testing 
```
To Deploy Frontend 
1. Go to docker-compose.yml and comment out frontend-server
2. Open a terminal and type, "yarn install" (If you do not already have yarn)
3. "yarn build" 
4. "yarn serve" 

To Deploy Backend 
1. Make sure that docker is running 
2. open a terminal and type, "docker-compose build"
3. Once build is complete type, "docker-compose up"

This should let the project run on local yarn port 

```
## Using Docker for Testing Front and Backend 
```
To deploy front and backend 
1. Make sure that docker is running 
2. If fronted-server is commented out, uncomment it 
3. Open a terminal and type, "docker-compose build"
4. Once build is complete type, "docker-compose up"
```
## Takedown Project 
```
Using Yarn
1. Open terminal where yarn is running and press, "ctrl + c"
This should shut down the server

Using Docker
1. Open terminal where docker is running and press, "ctrl + c"
2. Type, "docker-compose down"
This will terminate and take down docker images, make sure when relaunching project 
to type "docker-compose build" each time 
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).